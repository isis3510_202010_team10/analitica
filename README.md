# Analitica para FlothMark
# Instrucciones
<h4>Instrucciones de descarga y despliegue:</h4>
<ol>
  <li>Clonar el repositorio <a href="https://gitlab.com/isis3510_202010_team10/analitica">aqui</a></li>
  <li>Instalar dependencias ejecutando en consola y/o terminal el comando "npm install" en los directorios back y front ubicados en back y back/front respectivamente</li>
  <li>Ejecutar el proyecto ejecutando el comando "npm start" en ambas terminales (una sobre back y otra sobre back/front)</li>
  <li>Abrir en un navegador la dirección localhost:3000</li>
</ol>
