import React from 'react';
import logo from './logo.svg';
import './App.css';
import { render } from '@testing-library/react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Categoria from "./categoria";
import {Pie} from "react-chartjs-2";
import {
  XYPlot,
  XAxis,
  YAxis,
  VerticalGridLines,
  HorizontalGridLines,
  VerticalBarSeries,
  VerticalRectSeries,
} from 'react-vis';


class App extends React.Component{

  constructor(props){
    super(props);
    this.state={
      categorias:[],
      tiendas:[],
      productos:[],
      usuarios:[],
      todosProductos:[],
  };
  this.renderCategories = this.renderCategorias.bind(this);
}
componentDidMount(){
    fetch("http://localhost:3001/categories")
    .then(response => response.json())
    .then(data => this.setState({ categorias: data }))
    .catch(console.error);

    fetch("http://localhost:3001/stores")
    .then(response => response.json())
    .then(data => this.setState({ tiendas: data }));

    fetch("http://localhost:3001/products")
    .then(response => response.json())
    .then(data => this.setState({ productos: data }));

    fetch("http://localhost:3001/users")
    .then(response => response.json())
    .then(data => this.setState({ usuarios: data }));

    fetch("http://localhost:3001/allProducts")
    .then(response => response.json())
    .then(data => this.setState({ todosProductos: data }));
  }
  renderCategorias2() {
    if (this.state.categorias.length > 0) {
      return this.state.categorias.map((categoria, i) => (
        <div className="col-md-4">
            <Categoria obj={categoria} key={i} />
        </div>
      ));
    }
  }

  renderBarras(){
    if(this.state.todosProductos.length>0){
      console.log(this.state.todosProductos)
      var barras=[]
      for(var i=0;i< this.state.todosProductos.length;i++)
      {
        if(this.state.todosProductos[i]>0)
        {
          barras.push({x:i,y:this.state.todosProductos[i]});
        }
        
      }
    return(
      <XYPlot xType="ordinal" width={600} height={300} >
        <VerticalGridLines />
        <HorizontalGridLines />
        <XAxis />
        <YAxis />
        <VerticalBarSeries 
          color="#f00"
          data={barras}
        />
      </XYPlot>
    )}
  }
  renderCategorias() {
    return(
        <Pie
          data={{
            labels:this.state.categorias.slice(0, 5).map (
              cat => cat.Nombre),
            datasets:[{data:this.state.categorias.slice(0, 5).map (
              cat => cat.Consultas),backgroundColor:['red','blue','green','yellow','pink']}]
          }}
        />
  )}
  renderTiendas() {
    return(
        <Pie
          data={{
            labels:this.state.tiendas.slice(0, 10).map (
              sto => sto.Store_number),
            datasets:[{data:this.state.tiendas.slice(0, 10).map (
              sto => sto.Consultas),backgroundColor:['red','blue','green','yellow','pink', 'orange', 'purple', 'brown', 'gray', 'olive']}]
          }}
        />
  )}
  renderProductos() {
    return(
        <Pie
          data={{
            labels:this.state.productos.slice(0, 10).map (
              pro => pro.nombre),
            datasets:[{data:this.state.productos.slice(0, 10).map (
              pro => pro.consultas),backgroundColor:['red','blue','green','yellow','pink', 'orange', 'purple', 'brown', 'gray', 'olive']}]
          }}
        />
  )}
  render(){
    return (
      <div className="App">
        <div className="container">
            <div className="row">
              <div className="col-6">
                <h3> Categorias más buscadas </h3> 
                <div>{this.renderCategorias()}</div>
              </div>
              <div className="col-6">
                <h3> Tiendas más buscadas </h3> 
                <div>{this.renderTiendas()}</div>
              </div>
              <div className="col-6">
                <h3> Productos más buscados </h3> 
                <div>{this.renderProductos()}</div>  
              </div>
            </div>
            <div className="row">
              <div className="col-4">
                <h3> Numero de productos por tienda </h3> 
                <div>{this.renderBarras()}</div>
              </div>
            </div>
          </div>
      </div>
    );
  }
}

export default App;
