var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');

var app = express();

var firebase = require("firebase/app");
var firestore = require("firebase/firestore");
var admin = require('firebase-admin')
let serviceAccount = require('./flohmarkt-6307c-firebase-adminsdk-kq8un-ef967c10d0.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
  });

var firebaseConfig = {
    apiKey: "AIzaSyC4NcWAHZ4c5-oSNUPjM7Fz9HpzT25FIpY",
      authDomain: "flohmarkt-6307c.firebaseapp.com",
      databaseURL: "https://flohmarkt-6307c.firebaseio.com",
      projectId: "flohmarkt-6307c",
      storageBucket: "flohmarkt-6307c.appspot.com",
      messagingSenderId: "315116453077",
      appId: "1:315116453077:web:a5a9118fa76e557988765c"
  };
firebase.initializeApp(firebaseConfig);


var indexRouter = require('./routes/index');
var categoriesRouter = require('./routes/categories');
var productsRouter = require('./routes/products');
var storesRouter = require('./routes/stores');
var usersRouter = require('./routes/users')
var allProductsRouter = require ('./routes/allProducts')

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', indexRouter);
app.use('/categories', categoriesRouter);
app.use('/stores', storesRouter);
app.use('/products', productsRouter);
app.use('/users', usersRouter);
app.use('/allProducts', allProductsRouter)


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
